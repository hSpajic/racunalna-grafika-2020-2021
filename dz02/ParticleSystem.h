//
// Created by johnq on 12/10/20.
//

#ifndef DZ02_PARTICLESYSTEM_H
#define DZ02_PARTICLESYSTEM_H

#include "glm/ext.hpp"
#include "Particle.h"
#include <list>
#include <vector>

class ParticleSystem {
private:
	int max_particle_number;
	/** In degrees */
	float max_angle;
	glm::vec3 position;
	glm::vec3 direction;
	float rotation_speed;
	float translation_speed;

	std::vector<Particle *> particles;
	std::list<Particle *> particleCache;
public:
	ParticleSystem(int maxParticleNumber, float maxAngle,
				   const glm::vec3 position, const glm::vec3 direction,
				   float rotationSpeed, float translationSpeed);
	void update(float dt, glm::vec3 acc);
	void draw();

	const glm::vec3 &getPosition() const;
};


#endif //DZ02_PARTICLESYSTEM_H
