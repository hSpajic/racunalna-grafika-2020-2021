//
// Created by johnq on 12/9/20.
//

#ifndef DZ02_TEXTUREUTIL_HPP
#define DZ02_TEXTUREUTIL_HPP

#include <GL/glut.h>
#include <cstdio>
#include <iostream>

class TextureUtil {
public:
	static GLuint LoadBMPTexture(const char * filename, int width, int height)
	{
		unsigned char * data;

		FILE * file;
		file = fopen( filename, "rb" );

		if ( file == NULL ) {
			std::cerr << "Ne mogu otvoriti file!" << std::endl;
			return 0;
		}
		data = (unsigned char *)malloc( width * height * 3 );
		//int size = fseek(file,);
		fread( data, width * height * 3, 1, file );
		fclose( file );

		for(int i = 0; i < width * height ; ++i)
		{
			int index = i*3;
			unsigned char B,R;
			B = data[index];
			R = data[index+2];

			data[index] = R;
			data[index+2] = B;
		}

		GLuint texture;
		glGenTextures( 1, &texture );
//		glBindTexture( GL_TEXTURE_2D, texture );
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);

		free( data );

		return texture;
	}

	static void checkerTexture(GLuint * texName, int checkImageHeight, int checkImageWidth) {
		int i, j, c;
		GLubyte checkImage[checkImageHeight][checkImageWidth][4];

		for (i = 0; i < checkImageHeight; i++) {
			for (j = 0; j < checkImageWidth; j++) {
				c = ((((i&0x8)==0)^((j&0x8))==0))*255;
				checkImage[i][j][0] = (GLubyte) c;
				checkImage[i][j][1] = (GLubyte) c;
				checkImage[i][j][2] = (GLubyte) c;
				checkImage[i][j][3] = (GLubyte) 255;
			}
		}

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glGenTextures(1, texName);
		glBindTexture(GL_TEXTURE_2D, *texName);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
						GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
						GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, checkImageWidth,
					 checkImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE,
					 checkImage);

	}

};

#endif //DZ02_TEXTUREUTIL_HPP
