#define GLM_ENABLE_EXPERIMENTAL

#include "CameraPosition.hpp"
#include <iostream>
#include <algorithm>
#include <glm/gtx/string_cast.hpp>

CameraPosition::CameraPosition(vec3 p, vec3 target, float ts, float rs) {
	position = p;
	view_direction = glm::normalize(target - position); //x
	cam_right = normalize(cross(up, view_direction));
	cam_up = cross(view_direction, cam_right);
//	view_direction = glm::normalize(target);
	translation_speed = ts;
	rotation_speed = rs;


	//	view_direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	//	view_direction.y = sin(glm::radians(pitch));

	pitch = 3.14f - asin(view_direction.y);
	yaw = 3.14 + acos(view_direction.x/cos(glm::radians(pitch)));
}

void CameraPosition::to_camera_translate(int dx) {
	position += view_direction * (translation_speed * ((float) dx));
}

void CameraPosition::left_right_translate(int right_diff) {
	position += cam_right * (translation_speed * ((float) right_diff));
}

void CameraPosition::rotate(int dyaw, int dpitch) {
	yaw += rotation_speed * dyaw;
	pitch += rotation_speed * dpitch;

	if(pitch >= 89.0f) pitch = 89.0f;
	else if(pitch <= -89.0f) pitch = -89.0f;

//	std::cout << view_direction.x << " " << view_direction.y << "  " << view_direction.z << std::endl;

	view_direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	view_direction.y = sin(glm::radians(pitch));
	view_direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));

	view_direction = glm::normalize(view_direction);
	cam_right = normalize(cross(up, view_direction));
	cam_up = cross(view_direction, cam_right);

//	std::cout << yaw << " " << pitch << std::endl;
}

void CameraPosition::set_view() {

//	std::cout << position.x << " " << position.y << "  " << position.z << std::endl;

	cam_right = normalize(cross(up, view_direction));
	cam_up = cross(view_direction, cam_right);
	gluLookAt(
			position.x, position.y, position.z,
			view_direction.x + position.x, view_direction.y + position.y, view_direction.z + position.z,
			cam_up.x, cam_up.y, cam_up.z
	);



}


