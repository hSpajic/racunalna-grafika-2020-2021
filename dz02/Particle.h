//
// Created by johnq on 12/10/20.
//

#ifndef DZ02_PARTICLE_H
#define DZ02_PARTICLE_H

#include "glm/ext.hpp"

class Particle
{
private:
	glm::vec3 position;
	glm::vec3 speed;
	glm::vec4 rgba;
	float radius;
	float ttl;
	float alpha_decay = 1.0/255;
	Particle()= default;;
	Particle(glm::vec3, glm::vec3, glm::vec4, float, float);
public:
	static const int MAX_TTL = 255;
//	static Particle * from(glm::vec3, glm::vec3, glm::vec4, float, float);
//	static Particle * from(Particle *, glm::vec3, glm::vec3, glm::vec4, float, float);
	bool used = false;
	void update(float, glm::vec3);
	bool isDead() {
		return ttl < 1e-5;
	}

	const glm::vec4 &getRgba() const;

	const glm::vec3 &getPosition() const;

	static Particle *new_random(float max_angle, glm::vec3 direction, glm::vec3 position);

	static Particle *new_random(Particle *p, float angle, glm::vec3 direction, glm::vec3 position);
};

#endif //DZ02_PARTICLE_H
