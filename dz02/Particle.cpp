//
// Created by johnq on 12/10/20.
//
#define GLM_ENABLE_EXPERIMENTAL

#include <iostream>
#include "Particle.h"
#include "glm/gtx/norm.hpp"

Particle::Particle(glm::vec3 position, glm::vec3 speed, glm::vec4 rgba, float radius, float ttl)
 : position(position), speed(speed), rgba(rgba), radius(radius), ttl(ttl)
{

}

void second_pull(glm::vec3 & acc, glm::vec3 & pos, float delta_t) {
	float K = 0.0003;
	glm::vec3 delta = (glm::vec3(5,0,5) - pos) * K;

	acc.x += delta.x;
	acc.y += delta.y;
	acc.z += delta.z;
}

void Particle::update(float delta_t, glm::vec3 acceleration)
{
	second_pull(acceleration, position, delta_t);
	ttl -= delta_t;
	rgba.x = ttl/50.0;
	rgba.y = ttl/50.0;
	speed += acceleration * delta_t;
	position += speed * delta_t;
	if(position.y <= 1e-6) position.y = 0;

//	if(position.y <= 1e-6) {
//		ttl = -1;
//	}
}



//Particle * Particle::from(glm::vec3 position, glm::vec3 speed, glm::vec4 rgba, float radius, float ttl) {
//	Particle *p = new Particle();
//	return Particle::from(p, position, speed, rgba, radius, ttl);
//}
//
//Particle * Particle::from(Particle *p, glm::vec3 position, glm::vec3 speed, glm::vec4 rgba, float radius, float ttl) {
//	p->position = position;
//	p->speed = speed;
//	p->rgba = rgba;
//	p->radius = radius;
//	p->ttl = ttl;
//	return p;
//}

Particle *Particle::new_random(float max_angle, glm::vec3 direction, glm::vec3 position) {
	auto * p = new Particle();
	return Particle::new_random(p, max_angle, direction, position);
}


Particle *Particle::new_random(Particle *p, float max_angle, glm::vec3 direction, glm::vec3 position) {
	float radius = glm::l2Norm(direction) * 5;
	p->radius = 1;
//	p->rgba = glm::vec4(((float)rand())/RAND_MAX,((float)rand())/RAND_MAX,((float)rand())/RAND_MAX,1);
	p->rgba = glm::vec4(1, 0,0,1);

	float r = sqrt(((float)rand())/RAND_MAX);
	float theta = ((float)rand())/RAND_MAX* 6.28;

	float x = r * cos(theta);
	float y = r * sin(theta);

	p->speed = glm::normalize(glm::vec3(x,1, y)) / 10;
	p->ttl=50;
	p->used=true;
	p->position=position;
	return p;
}

const glm::vec3 &Particle::getPosition() const {
	return position;
}

const glm::vec4 &Particle::getRgba() const {
	return rgba;
}

