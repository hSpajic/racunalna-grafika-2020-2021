//
// Created by johnq on 12/10/20.
//

#include <GL/gl.h>
#include "ParticleSystem.h"

ParticleSystem::ParticleSystem(int maxParticleNumber,
							   float maxAngle,
							   const glm::vec3 position,
							   const glm::vec3 direction,
							   float rotationSpeed,
							   float translationSpeed)
						   : max_particle_number(maxParticleNumber), max_angle(maxAngle),
							 position(position), direction(direction),
							 rotation_speed(rotationSpeed), translation_speed(translationSpeed) {

	particles.reserve(max_particle_number);

	for(int i = 0; i < max_particle_number; ++i) {
		Particle *p = Particle::new_random(max_angle, direction, position);
		p->used = false;
		particles.push_back(p);
		particleCache.push_front(p);
	}

}

void ParticleSystem::update(float dt, glm::vec3 acc) {
	for(auto v : particles) {
		if(v->used) {
			v->update(dt, acc);
		}
	}
	//dodaj nove
	for(int i = 50; i-->0;) {
		if (!particleCache.empty()) {
			Particle *p = particleCache.front();
			particleCache.pop_front();
			p->used = true;
			Particle::new_random(p, max_angle, direction, position);
		}
	}

	//izbaci stare
	for(auto v : particles) {
		if(v->isDead()) {
			v->used = false;
			particleCache.push_back(v);
		}
	}

}

void ParticleSystem::draw() {
	glPointSize(3);
	glBegin(GL_POINTS);
	for(auto v : particles) {
		if(!v->used) continue;
		auto pos = v->getPosition();
		auto col = v->getRgba();
		glColor4f(col.x, col.y, col.z, col.w);
		glVertex3f(pos.x, pos.y, pos.z);
	}
	glEnd();
}

const glm::vec3 &ParticleSystem::getPosition() const {
	return position;
}



