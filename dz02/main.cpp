#define GLM_ENABLE_EXPERIMENTAL

#include <iostream>
#include <GL/glut.h>
#include "glm/ext.hpp"

#include "CameraPosition.hpp"
#include "TextureUtil.hpp"
#include "ParticleSystem.h"
#include <random>

#include "vector"

CameraPosition camera_position(
		glm::vec3(-2, 2, 2),	//position
		glm::vec3(0, 0, 0),		//target
		0.1, 1						//transl and rot speed
	);

void display();

void draw_floor();

void myKeyboard(unsigned char theKey, int mouseX, int mouseY) {

	if (theKey == 27) {
		exit(0);
	}

	switch (theKey) {
		case 'w':
			camera_position.to_camera_translate(1);
			break;
		case 's':
			camera_position.to_camera_translate(-1);
			break;
	}

	switch(theKey) {
		case 'a':
			camera_position.left_right_translate(1);
			break;
		case 'd':
			camera_position.left_right_translate(-1);
			break;
	}

	switch(theKey) {
		case 'q':
			camera_position.rotate(-1, 0);
			break;
		case 'e':
			camera_position.rotate(1,0);
			break;
		case 'i':
			camera_position.rotate(0, 1);
			break;
		case 'k':
			camera_position.rotate(0, -1);
			break;
	}

	display();
}

static GLuint texName;



void initGL() {
	glClearColor(0.750f, 0.750f, 0.750f, 1.0f); // Set background color to silver and opaque
//	glClearDepth(1.0f);                   // Set background depth to farthest
	glEnable(GL_DEPTH_TEST);
	glEnable( GL_TEXTURE_2D);
	glDepthFunc(GL_LEQUAL);    // Set the type of depth-test
	// glShadeModel(GL_SMOOTH);   // Enable smooth shading
	// glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections
	glutKeyboardFunc(myKeyboard);

	//stvori teksture
	TextureUtil::checkerTexture(&texName, 64, 64);

//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//	glEnable( GL_BLEND );

//	grass_texture = TextureUtil::LoadBMPTexture("../textures/grass.bmp", 512, 512);
}

void reshape(GLsizei width, GLsizei height) {  // GLsizei for non-negative integer
	// Compute aspect ratio of the new window
	if (height == 0) height = 1;                // To prevent divide by 0
	GLfloat aspect = (GLfloat)width / (GLfloat)height;

	// Set the viewport to cover the new window
	glViewport(0, 0, width, height);

	// Set the aspect ratio of the clipping volume to match the viewport
	glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
	glLoadIdentity();             // Reset
	// Enable perspective projection with fovy, aspect, zNear and zFar
	gluPerspective(30.0f, aspect, 0.1f, 1000.0f);
}


//int maxParticleNumber, float maxAngle,
//				   const glm::vec3 position, const glm::vec3 direction,
//				   float rotationSpeed, float translationSpeed
ParticleSystem particleSystem(3000, 10.f,
							  glm::vec3(0,0,0),
							  glm::vec3(0,1,0),
							  10,
							  10
							  );

void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear color and depth buffers
	glMatrixMode(GL_MODELVIEW);     // To operate on model-view matrix
	glLoadIdentity();
	camera_position.set_view();

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texName);
	//crtaj nesto
	draw_floor();
//	draw_cube();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPushMatrix();
	particleSystem.update(0.1, glm::vec3(0,0,0));
	particleSystem.draw();
	glPopMatrix();

//	glPushMatrix();
//	glPointSize(5);
//	glBegin(GL_POINTS);
//	glVertex3f(3,3,3);
//	glPopMatrix();
//	glEnd();

	glutSwapBuffers();
}

void draw_floor() {

	glBegin(GL_TRIANGLES);                // Begin drawing the color cube with 6 quads
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0, 0);
	glVertex3f(-5,0, -5);
	glTexCoord2f(1, 0);
	glVertex3f(5, 0,-5);
	glTexCoord2f(0, 1);
	glVertex3f(-5, 0, 5);

	glTexCoord2f(1, 0);
	glVertex3f(5,0, -5);
	glTexCoord2f(0, 1);
	glVertex3f(-5,0, 5);
	glTexCoord2f(1, 1);
	glVertex3f(5, 0, 5);


	glEnd();  // End of drawing color-cube
}

//SmokeParticleSystem smokeParticleSystem();


int main(int argc, char **argv) {
	//stvori pocetne particle
	//uhvati keypress i postavi

	srand(1234);

	//load textures
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("2. lab vjezba");
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	initGL();
	glutIdleFunc(display);
	glutMainLoop();

	return 0;
}
