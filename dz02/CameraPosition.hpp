#ifndef RACGRA_01_CAMERAPOSITION_H
#define RACGRA_01_CAMERAPOSITION_H

#include "GL/glut.h"
//#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include "glm/glm.hpp"

#include <glm/common.hpp>


using namespace glm;

class CameraPosition {
	vec3 position;
	vec3 view_direction;
	vec3 cam_right;
	vec3 cam_up;

	/* Definira pojam "gore" u world spaceu */
	const vec3 up = vec3(0.f,1.f,0.f);

	float translation_speed;
	float rotation_speed;

	float yaw = -90.f;
	float pitch = 0.0f;
public:
	CameraPosition(vec3, vec3, float, float);
	void set_view();
	void to_camera_translate(int);

	void left_right_translate(int right_diff);

	void rotate(int dyaw, int dpitch);
};


#endif //RACGRA_01_CAMERAPOSITION_H
