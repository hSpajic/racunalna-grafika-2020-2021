# 3. vjezba - Reynoldsovi boidi

## Uvod
Cilj programa je prikazati kretanje Reynoldsovih boida i njihovo kretanje s obzirom 
na prepreke. U pokrenutom programu moguce je dodavati boide ili prepreke, te pokretati 
ili zaustavljati animaciju.

## Prevodjenje
Stvorite `build` direktorij u dz03.
U `build` direktoriju pokrenite:
 - `dz03/build/$ cmake ..`
 - `dz03/build/$ make`

## Pokretanje
Izvrsni program nalazi se u `build` direktoriju pod imenom `dz03`.
Programu nisu potrebni ulazni argumenti.

`dz03/$ ./build/dz03`

## Kontrole
Animaciju pokrecemo ili zaustavljamo pritiskom tipke `s`. Pritiskom razmaknice ("space") 
mijenjamo zelimo li u scenu postavljati prepreku ili boid. Smjer promjene pomaka pokazivaca
odredjuje i smjer postavljenog boida.

Pritiskom `ESC` izlazimo iz programa.

## Promjena parametara
Koheziju, separaciju, poravnanje i izbjegavanje prepreka moguce je mijenjati mijenjanjem 
koeficijenata navedenih u BoidSystem.cpp.

# Primjer pokretanja

Napomena: GIF ne radi u PDFu. GIF je moguce vidjeti u git repozitoriju pod imenom `dz3_animation.gif`.

![](dz3_animation.gif)