//
// Created by johnq on 1/12/21.
//

#ifndef DZ03_BOID_H
#define DZ03_BOID_H

#include "glm/glm.hpp"


class Boid {
public:
	/** x,y */
	glm::vec2 position;
	glm::vec2 orientation;
	/** RGBA */
	glm::ivec4 color;

	float size_scale;
	Boid(glm::ivec2 &position, glm::ivec2 &orientation, float sizeScale);

	glm::vec2 &getPosition();

	glm::vec2 &getOrientation();

	glm::ivec4 &getColor();

	float getSizeScale();

	void setPosition(const glm::vec2 &position);

	glm::highp_vec2 new_orientantion;
};


#endif //DZ03_BOID_H
