//
// Created by johnq on 1/12/21.
//

#include "Boid.h"

Boid::Boid(glm::ivec2 &position, glm::ivec2 &orientation, float sizeScale)
		: color(color), size_scale(sizeScale) {
	this->position = glm::vec2(position);
	this->orientation = glm::normalize(glm::vec2(orientation));
	this->new_orientantion = glm::vec2();
	this->color = glm::ivec4(1,0,0,1);
}

glm::vec2& Boid::getPosition() {
	return position;
}

glm::vec2 &Boid::getOrientation() {
	return orientation;
}

glm::ivec4 &Boid::getColor() {
	return color;
}

float Boid::getSizeScale() {
	return size_scale;
}

