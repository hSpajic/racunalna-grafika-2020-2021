//
// Created by johnq on 1/12/21.
//

#ifndef DZ03_DRAWINGSTRATEGY_H
#define DZ03_DRAWINGSTRATEGY_H

#include "MouseState.h"
#include "BoidSystem.h"

class DrawingStrategy {
public:
	static void drawMouse(MouseState& mouse_state);

	static void drawBoid(glm::vec2 &head, glm::vec2 &dir_f);

	static void drawBoidSystem(BoidSystem system);

	static void drawObstacle(glm::ivec2 * vec);
};


#endif //DZ03_DRAWINGSTRATEGY_H
