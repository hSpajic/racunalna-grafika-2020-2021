//
// Created by johnq on 1/12/21.
//

#define GLM_ENABLE_EXPERIMENTAL

#include "DrawingStrategy.h"
#include <glm/gtx/rotate_vector.hpp>
#include <GL/glut.h>
#include <glm/gtx/string_cast.hpp>
#include <iostream>

void DrawingStrategy::drawMouse(MouseState &mouse_state) {

	if(mouse_state.getDrawingState() == MouseDrawingState::BOID) {
		glm::vec2 head = glm::vec2(mouse_state.getPosition());
		glm::vec2 dir_f = glm::normalize(glm::vec2(mouse_state.getDirection()));
		DrawingStrategy::drawBoid(head, dir_f);
	} else if(mouse_state.getDrawingState() == MouseDrawingState::OBSTACLE) {
		glm::ivec2 head = glm::ivec2(mouse_state.getPosition());
		DrawingStrategy::drawObstacle(&head);
	}
}

void DrawingStrategy::drawBoid(glm::vec2 &head, glm::vec2 &dir_f) {
	float pointer_scale = 30.0;

	glPushMatrix();

	glm::vec2 perp;
	if(glm::abs(dir_f.x) >= 1e-5) {
		perp.y = 1;
		perp.x = -dir_f.y/dir_f.x;
	} else {
		perp.x = 1;
		perp.y = -dir_f.x/dir_f.y;
	}
	perp = glm::normalize(perp);
	perp.x = perp.x/5.0 * pointer_scale;
	perp.y = perp.y/5.0 * pointer_scale;

	glm::vec2 left = (head - dir_f * pointer_scale) + perp;
	glm::vec2 right = (head - dir_f * pointer_scale) - perp;

//	std::cout << glm::to_string(left) << std::endl;
//	std::cout << glm::to_string(right) << std::endl;
//	std::cout << "DIR: " << glm::to_string(dir_f) << std::endl;

	glColor3f(1, 0, 0);
	glBegin(GL_TRIANGLES);
	glVertex2f(head.x, head.y);
	glVertex2f(left.x, left.y);
	glVertex2f(right.x, right.y);
	glEnd();
	glPopMatrix();

}

void DrawingStrategy::drawBoidSystem(BoidSystem system) {
	for(auto& boid : system.getBoids()) {
		drawBoid(boid->getPosition(), boid->getOrientation());
	}

	for(auto obs : system.getObstacles()) {
		DrawingStrategy::drawObstacle(obs);
	}
}

void DrawingStrategy::drawObstacle(glm::ivec2 * vec) {
	glPushMatrix();
	glBegin(GL_QUADS);
		glColor4f(1,1,1,1);
		glVertex2f(vec->x - 10, vec->y - 10);
		glVertex2f(vec->x - 10, vec->y + 10);
		glVertex2f(vec->x + 10, vec->y - 10);
		glVertex2f(vec->x + 10, vec->y + 10);
	glEnd();
	glPopMatrix();
}

