//
// Created by johnq on 1/12/21.
//

#ifndef DZ03_MOUSESTATE_H
#define DZ03_MOUSESTATE_H

#include<glm/glm.hpp>

enum MouseDrawingState {BOID, OBSTACLE};

class MouseState {
private:
	glm::ivec2 position;
	glm::ivec2 previousPosition;
	MouseDrawingState drawing_state;
public:
	MouseDrawingState getDrawingState() ;

	void setDrawingState(MouseDrawingState drawingState);

public:
	MouseState();

	glm::ivec2 &getPosition() ;

	void setPosition(int x, int y);

	glm::ivec2 getDirection() ;

	void flipDrawing();
};

#endif //DZ03_MOUSESTATE_H