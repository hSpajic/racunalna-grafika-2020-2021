cmake_minimum_required(VERSION 3.17)
project(dz03)

set(CMAKE_CXX_STANDARD 14)

find_package(GLUT REQUIRED)
if(NOT ${GLUT_FOUND})
    message(FATAL_ERROR "GLUT not found!")
else()
    message(STATUS "GLUT Found")
endif()

find_package(OpenGL REQUIRED)
if(NOT ${OPENGL_GLU_FOUND})
    message(FATAL_ERROR, "GL not found!")
else()
    message(STATUS "GL Found")
endif()


add_executable(dz03 main.cpp Boid.cpp Boid.h Visitor.cpp Visitor.h Flock.cpp Flock.h MouseState.cpp MouseState.h DrawingStrategy.cpp DrawingStrategy.h BoidSystem.cpp BoidSystem.h)

target_include_directories(${PROJECT_NAME} PUBLIC ${GLUT_INCLUDE_DIR} ${OPENGL_INCLUDE_DIR})
target_link_libraries(${PROJECT_NAME} ${GLUT_LIBRARIES} ${OPENGL_LIBRARIES})
