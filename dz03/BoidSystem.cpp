//
// Created by johnq on 1/12/21.
//

#define GLM_ENABLE_EXPERIMENTAL

#include "BoidSystem.h"
#include <iostream>
#include <glm/gtx/string_cast.hpp>

/**
 * Razmisljanje o parametrima:
 * 	- kohezija bi trebala biti dalekoseznija od separacije i obstaclea, ali slabija
 * 	- obstc bi trebao biti jaci od separacije i kohezije, ali na manje udaljenosti
 *
 */

#define ALIGN_DIST 50
#define COHES_DIST 60
#define SEPAR_DIST 40
#define OBSTC_DIST 50

#define ALIGN_FACT 0.9
#define COHES_FACT 0.051
#define SEPAR_FACT 0.5
#define OBSTC_FACT 0.1

#define SPEED_FACT 0.05

#define MAX_SPEED 2

#define REALIGN_COUNT 5

void BoidSystem::createBoid(glm::ivec2& pos, glm::ivec2& dir) {
	//Boid(glm::ivec2 &position, glm::ivec2 &orientation, glm::ivec4 &color, float sizeScale);
	Boid *b;
	b = new Boid(pos, dir, 5.f);
	this->boids.push_back(b);
}

std::list<Boid *> &BoidSystem::getBoids() {
	return boids;
}

void BoidSystem::createObstacle(glm::ivec2 &vec) {
	this->obstacles.push_back(new glm::ivec2(vec));
}

const std::list<glm::ivec2 *> &BoidSystem::getObstacles() const {
	return obstacles;
}

void BoidSystem::update() {
	//2. za one dovoljno blizu da se svojstvo osjeti izracunaj vektor pomaka zbog prepreke
	//3.	-||- zbog kohezije (cohesion)
	//4.	-||- zbog odvajanja (separation)
	//5.	-||- zbog poravnanja (alignment)

	//1.
	for(auto boid : boids) {
		//odmah i cache ispravi
		boid->new_orientantion.x = boid->orientation.x;
		boid->new_orientantion.y = boid->orientation.y;

		glm::vec2 result = glm::vec2(0,0);
		for(auto obs : obstacles) {
			glm::vec2 diff_vec = boid->position - glm::vec2(*obs);
			float dist = glm::length(diff_vec);
			if(dist >= OBSTC_DIST) {
				continue;
			}
			result += diff_vec/(dist*dist); //obrnuto prop proporcionalno udaljenosti
		}
//		result = glm::normalize(result);
		boid->new_orientantion.x += result.x * OBSTC_FACT;
		boid->new_orientantion.y += result.y * OBSTC_FACT;
	}


	//2.-> 5.
	for(auto boid1 : boids) {
		glm::vec2 coh_result = glm::vec2(0, 0);
		glm::vec2 alg_result = glm::vec2(0, 0);
		glm::vec2 sep_result = glm::vec2(0, 0);

		int number_of_boids_in_alignment = 0;
		for (auto boid2 : boids) {
			if(boid1 == boid2) continue; //pointeri su pa je ovo oke

			glm::vec2 diff_vec = - boid1->position + boid2->position;
			float dist = glm::length(diff_vec);

			if(dist < COHES_DIST) { //idi prema jatu
				coh_result += diff_vec/(dist * dist);
				std::cout  << glm::to_string(diff_vec/dist) << std::endl;
			}
			if(dist < ALIGN_DIST) { //poravnaj smjer u prosjeku jata
				alg_result += boid1->orientation / (dist * dist);
				std::cout << "ALGN " << std::endl;
				++number_of_boids_in_alignment;
			}
			if(dist < SEPAR_DIST) { //odvajaj se od pojedinih
				sep_result -= diff_vec/(dist * dist);
				std::cout << "SEPA " << std::endl;
			}
		}

		boid1->new_orientantion.y += coh_result.y * COHES_FACT;
		boid1->new_orientantion.x += coh_result.x * COHES_FACT;

		if(number_of_boids_in_alignment > 0 && time % REALIGN_COUNT == 0) {
			boid1->new_orientantion.x += alg_result.x * ALIGN_FACT / ((float) number_of_boids_in_alignment);
			boid1->new_orientantion.y += alg_result.y * ALIGN_FACT / ((float) number_of_boids_in_alignment);
		}
		boid1->new_orientantion.x += sep_result.x * SEPAR_FACT;
		boid1->new_orientantion.y += sep_result.y * SEPAR_FACT;
	}

	//ucitaj cache
	for(auto boid : boids) {
		boid->orientation.x = boid->new_orientantion.x;
		boid->orientation.y = boid->new_orientantion.y;
		if(glm::length(boid->orientation) > MAX_SPEED) {
			std::cout << "Korekcija brzine" << std::endl;
			boid->orientation = glm::normalize(boid->orientation) * MAX_SPEED;
		} else if(glm::length(boid->orientation) < 1) {
			std::cout << "Korekcija brzine2" << std::endl;
			boid->orientation = glm::normalize(boid->orientation) ;
		}
		boid->orientation = glm::normalize(boid->orientation);
	}

	//azuriraj position
	for(auto boid : boids) {
		boid->position.x += boid->orientation.x * SPEED_FACT;
		boid->position.y += boid->orientation.y * SPEED_FACT;
	}

	++time;

}
