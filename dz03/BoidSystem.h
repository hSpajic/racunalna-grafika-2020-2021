//
// Created by johnq on 1/12/21.
//

#ifndef DZ03_BOIDSYSTEM_H
#define DZ03_BOIDSYSTEM_H

#include <list>
#include "glm/ext.hpp"
#include "Boid.h"


class BoidSystem {
private:
	std::list<Boid *> boids;
	std::list<glm::ivec2 *> obstacles;
	unsigned long time = 0;
public:
	const std::list<glm::ivec2 *> &getObstacles() const;

public:
	void createBoid(glm::ivec2& pos, glm::ivec2& dir);
	std::list<Boid *> &getBoids();

	void createObstacle(glm::ivec2 &vec);

	void update();
};


#endif //DZ03_BOIDSYSTEM_H
