//
// Created by johnq on 1/12/21.
//

#include "MouseState.h"

MouseState::MouseState() {}

glm::ivec2 &MouseState::getPosition()  {
	return position;
}

void MouseState::setPosition(int x, int y) {
	if(position.x == x && position.y == y) return;

	this->previousPosition = this->position;
	this->position = glm::ivec2(x, y);
}

glm::ivec2 MouseState::getDirection() {
	if(position.x == previousPosition.x && position.y == previousPosition.y) {
		return glm::ivec2(1,0);
	}
	return position - previousPosition;
}

MouseDrawingState MouseState::getDrawingState() {
	return drawing_state;
}

void MouseState::setDrawingState(MouseDrawingState drawingState) {
	drawing_state = drawingState;
}

void MouseState::flipDrawing() {
	drawing_state = (drawing_state == MouseDrawingState::OBSTACLE) ? MouseDrawingState::BOID : MouseDrawingState::OBSTACLE;
}

