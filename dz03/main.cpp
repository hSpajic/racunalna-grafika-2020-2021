#include <iostream>
#include <GL/glut.h>
#include "glm/ext.hpp"
#include "MouseState.h"
#include "DrawingStrategy.h"
#include "BoidSystem.h"

#define WIN_HEIGHT 900
#define WIN_WIDTH 900


MouseState mouse_state;
BoidSystem boidSystem;
bool animation_running;

void display() {
	glClear(GL_COLOR_BUFFER_BIT);         // Clear the color buffer (background)

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	DrawingStrategy::drawMouse(mouse_state);
	DrawingStrategy::drawBoidSystem(boidSystem);

	if(animation_running) {
		boidSystem.update();
	}

	glFlush();
}

void reshape(GLsizei width, GLsizei height) {  // GLsizei for non-negative integer
	// Compute aspect ratio of the new window
	if (height == 0) height = 1;                // To prevent divide by 0
	GLfloat aspect = (GLfloat) width / (GLfloat) height;

	// Set the viewport to cover the new window
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
	glLoadIdentity();             // Reset the projection matrix
	gluOrtho2D(0, width, 0, height);
}


void myKeyboard(unsigned char theKey, int mouseX, int mouseY) {

	if (theKey == 27) {
		exit(0);
	}
	if (theKey == ' ') {
		mouse_state.flipDrawing();
	}

	if (theKey == 's') {
		animation_running = !animation_running;
		std::cout << ((animation_running) ? "pokrecem" : "zaustavljam") <<  " animaciju." << std::endl;
	}

}

void initGL() {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set background color to black and opaque
	glClear(GL_COLOR_BUFFER_BIT);         // Clear the color buffer (background)
}

void mouse_movement_handler(int x, int y) {
//	std::cout << x << " " << y << std::endl;
	mouse_state.setPosition(x, WIN_HEIGHT - y);
}

void mouse_func(int button, int state, int x, int y) {
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		if(mouse_state.getDrawingState() == MouseDrawingState::BOID) {
			glm::ivec2 vvv = mouse_state.getDirection();
			boidSystem.createBoid(mouse_state.getPosition(), vvv);
		} else {
			boidSystem.createObstacle(mouse_state.getPosition());
		}
	}
}

int main(int argc, char **argv) {

	std::cout << "Pritisni 'space' za promjenu objekta kojeg postavljas." << std::endl;
	std::cout << "Pritisni 's' za pokretanje ili zaustavljanje animacije." << std::endl;

	glutInit(&argc, argv);
	glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("3. laboratorijska vjezba - Boidi");
	glutDisplayFunc(display);
	glutPassiveMotionFunc(mouse_movement_handler);
	glutKeyboardFunc(myKeyboard);
	glutReshapeFunc(reshape);
	glutIdleFunc(display);
	glutMouseFunc(mouse_func);
	initGL();
	glutIdleFunc(display);
	glutMainLoop();

	return 0;
}
