#include "object_util.hpp"

#include <fstream>
#include <iterator>
#include <sstream>

void parse_vec3_a(vector<string> tokens, vector<vec3> * storage);

void parse_path_vertices(string path, vector<vec3> * vertices) {
    ifstream file;
    file.open(path);

    string line;

    while (getline(file, line) ) {
        istringstream buf(line);
        istream_iterator<string> beg(buf), end;

        vector<string> tokens(beg, end);
        parse_vec3_a(tokens, vertices);
    }

    file.close();
}

void parse_vec3_a(vector<string> tokens, vector<vec3> * storage) {
    float v1 = stof(tokens.at(0));
    float v2 = stof(tokens.at(1));
    float v3 = stof(tokens.at(2));

    vec3 result = vec3(v1, v2, v3);
    storage->push_back(result);
}
