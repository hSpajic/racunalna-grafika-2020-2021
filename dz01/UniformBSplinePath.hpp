#ifndef UNIFORM_B_SPLINE_CLASS_HPP
#define UNIFORM_B_SPLINE_CLASS_HPP

#include "UniformBSplinePath.hpp"
#include "path_util.hpp"

#include <vector>
#include <string>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat3x4.hpp>
#include <glm/mat4x3.hpp>
#include <glm/mat4x2.hpp>
#include <glm/common.hpp>

using namespace std;

using vec2 = glm::vec2;
using vec3 = glm::vec3;
using vec4 = glm::vec4;
using mat4 = glm::mat4;
using mat3x4 = glm::mat3x4;
using mat4x3 = glm::mat4x3;
using mat4x2= glm::mat4x2;

double PATH_STEP = 0.01;

mat4 B = mat4(
        vec4(-1, 3, -3, 1),
        vec4(3, -6, 0, 4),
        vec4(-3, 3, 3, 1),
        vec4(1, 0, 0, 0)
    );

mat4x3 dB = mat4x3(
		vec3(-1, 2, -1),
		vec3(3, -4, 0),
		vec3(-3, 2, 1),
		vec3(1, 0, 0)
	);

mat4x2 ddB = mat4x2(
		vec2(-1, 1),
		vec2(3, -2),
		vec2(-3, 1),
		vec2(1, 0)
	);


vec4 T(double t) {
    return vec4(pow(t, 3), pow(t, 2), t, 1);
}

vec3 Td(double t) {
	return vec3(pow(t, 2), t, 1);
}


class UniformBSplinePath {
    vector<vec3> points_;
    vector<vec3> path_;
    private :
        void calc_path();
    public:
        UniformBSplinePath (string file_path);
        vector<vec3> points() {return points_;}
        vec3 evaluate(double t, int i);
        vec3 derivation(double t, int i);
        vec3 second_derivation(double t, int i);
        vector<vec3> path() {
            return path_;
        }
};

UniformBSplinePath::UniformBSplinePath(string file_path) {
    parse_path_vertices(file_path, &points_);
    this->calc_path();
}

void UniformBSplinePath::calc_path() {
    for (int i = 1; i < points_.size() - 2; i++) {
        for (double t = 0; t < 1.0; t += PATH_STEP) {
            path_.push_back(this->evaluate(t, i));
        }
    }
}

vec3 UniformBSplinePath::evaluate(double t, int i) {
    vec4 T_value = T(t);

    mat3x4 R = glm::transpose(mat4x3(
                points_.at(i-1), points_.at(i), points_.at(i+1), points_.at(i+2)
            ));

    return T_value * (1.0/6) * B * R;
}

vec3 UniformBSplinePath::derivation(double t, int i) {
	mat3x4 R = transpose(mat4x3(
			points_.at(i-1), points_.at(i), points_.at(i+1), points_.at(i+2)
	));
	return Td(t) * 0.5f * dB * R;
}

vec3 UniformBSplinePath::second_derivation(double t, int i) {
	vec2 Tdd = vec2(t, 1);
	mat3x4 R = transpose(mat4x3(
			points_.at(i-1), points_.at(i), points_.at(i+1), points_.at(i+2)
	));

	return Tdd * ddB * R;
}

#endif