#ifndef PATH_UTIL_HPP
#define PATH_UTIL_HPP

#include <vector>
#include <glm/vec3.hpp>

void parse_path_vertices(string path, vector<vec3> * vertices);

#endif