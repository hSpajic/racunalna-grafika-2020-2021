#include "object_util.hpp"

#include <iterator>
#include <sstream>

void parse_line(string line, vector<vec3> * vertices, vector<vec3> * connections);

void parse_vec3(vector<string> tokens, vector<vec3> * storage);

void parse_3_int(vector<string> tokens, vector<vec3> * storage);

void parse_data(ifstream * file_stream, vector<glm::vec3> * vertices, vector<glm::vec3> * connections) {
    string line;
    while (getline(*file_stream, line) ) {
        parse_line(line, vertices, connections);
    }

    return ;
}

vector<string> tokenize(string str) {
    istringstream buf(str);
    istream_iterator<string> beg(buf), end;

    return vector<string>(beg, end);
}


void parse_line(string line, vector<vec3> * vertices, vector<vec3> * connections) {
    vector<string> tokens = tokenize(line);

    if(tokens.empty()) {
        return ;
    }

    string token = tokens.at(0);
    if(token == "#"){
        return;
    } 
    if (token == "v") {
        parse_vec3(tokens, vertices);
        return ;
    }
    if (token == "f") {
        parse_3_int(tokens, connections);
        return ;
    }

    return ;
}

void parse_vec3(vector<string> tokens, vector<vec3> * storage) {
    float v1 = stof(tokens.at(1));
    float v2 = stof(tokens.at(2));
    float v3 = stof(tokens.at(3));

    vec3 result = vec3(v1, v2, v3);
    storage->push_back(result);
}

void parse_3_int(vector<string> tokens, vector<vec3> * storage) {
    int v1 = stoi(tokens.at(1));
    int v2 = stoi(tokens.at(2));
    int v3 = stoi(tokens.at(3));

    storage->push_back(vec3(v1, v2, v3));
}
