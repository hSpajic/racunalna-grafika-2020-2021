//
// Created by johnq on 10/25/20.
//

#ifndef RACGRA_01_ANIMATIONSTATE_H
#define RACGRA_01_ANIMATIONSTATE_H


#include <jmorecfg.h>

class AnimationState {
	int seg_ = 1;
	int lap_ = 0;
	double t_ = 0.0;
	double T_INCREMENT = 0.01;
	double T_MAX = 1.0;
	int SEG_MAX = -1;
	boolean showPlane_ = true;
	boolean animationRuns_ = showPlane_;
public:
	void next() {
		if(!animationRuns_) return ;
		lap_++;
		t_ += T_INCREMENT;
		if(t_ >= T_MAX) {
			t_ = 0.0;
			seg_++;
			if(seg_ == SEG_MAX) {
				seg_ = 0;
			}
		}
	}

	boolean getShowPlane() const {
		return showPlane_;
	}

	int getSeg() const {
		return seg_;
	}

	void setSegMax(int segMax) {
		SEG_MAX = segMax;
	}

	double getT() const {
		return t_;
	}

	int getLap() const {
		return lap_;
	}

	void flipShowPlane() {
		showPlane_ = !showPlane_;
	}

	void flipAnimationRuns() {
		animationRuns_ = !animationRuns_;
	}
};


#endif //RACGRA_01_ANIMATIONSTATE_H
