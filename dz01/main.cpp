#define GLM_ENABLE_EXPERIMENTAL


#include<GL/glut.h>  // GLUT, include glu.h and gl.h
#include <iostream>
#include "glm/ext.hpp"
#include "glm/common.hpp"
#include "object_util.hpp"
#include "path_util.hpp"
#include "UniformBSplinePath.hpp"
#include "CameraPosition.h"
#include "AnimationState.h"
#include <math.h>

using namespace glm;
using namespace std;

// double angle = 0;

boolean USE_DCM_FLAG = false;

void display();

void show_plane(vec3 CURRENT_POINT, mat3 rotation);

mat3 calc_show_orientation(vec3 current_point);

void show_spiral();

float *to_array(mat3 mat);

vector<vec3> vertices;
vector<vec3> connections;

vector<vec3> path_vertices;

UniformBSplinePath * spiral;

char title[] = "Kretanje po krivulji";

CameraPosition camera_position(vec3(-5, -5, -50), vec3(0, 0, 0), 0.01, 0.01);

AnimationState animationState;

void show_plane_w_direction(vec3 current_point, vec3 direction);

void myKeyboard(unsigned char theKey, int mouseX, int mouseY) {
	switch (theKey) {
		case 'w':
			camera_position.translate(0, 0, -1);
			break;
		case 'a':
			camera_position.translate(-1, 0, 0);
			break;
		case 's':
			camera_position.translate(0, 0, 1);
			break;
		case 'd':
			camera_position.translate(1, 0, 0);
			break;
			// case 'i':
			//    angle += 1;
			//   break;
			// case 'k':
			//    angle -= 1;
			//    break;
		case 'p':
			animationState.flipShowPlane();
			break;
		case ' ':
			animationState.flipAnimationRuns();
			break;
		case 27:
			exit(0);
			break;

	}
	display();
}

void myMouseClickFunction(int button, int state, int x, int y) {
	cout << "Klik: " << button << endl;
}

void myMouseMotionFunction(int x, int y) {
	cout << x << y << endl;
}

void idleFunction() {
	animationState.next();
	display();
}

void initGL() {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set background color to black and opaque
	glClearDepth(1.0f);                   // Set background depth to farthest
	glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
	// glDepthFunc(GL_LEQUAL);    // Set the type of depth-test
	// glShadeModel(GL_SMOOTH);   // Enable smooth shading
	// glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections
	glutKeyboardFunc(myKeyboard);
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear color and depth buffers
	glMatrixMode(GL_MODELVIEW);     // To operate on model-view matrix
	glLoadIdentity();
	camera_position.set_view();

	vec3 CURRENT_POINT = spiral->path().at(animationState.getLap());

	glPushMatrix();
	show_spiral();
	glPopMatrix();

	if(USE_DCM_FLAG) {
		//nacrtaj orijentaciju
		glPushMatrix();
		mat3 rotation = calc_show_orientation(CURRENT_POINT);
		glPopMatrix();

		if (animationState.getShowPlane()) {
			glPushMatrix();
			show_plane(CURRENT_POINT, rotation);
			glPopMatrix();
		}
	} else {
		glPushMatrix();
		glTranslatef(CURRENT_POINT.x, CURRENT_POINT.y, CURRENT_POINT.z);
		vec3 first_der = glm::normalize(spiral->derivation(animationState.getT(), animationState.getSeg()));
		glScaled(2,2,2);

		glColor3f(1,1,1);
		glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(first_der.x, first_der.y, first_der.z);
		glEnd();

		glPopMatrix();

		if(animationState.getShowPlane()) {
			glPushMatrix();
			show_plane_w_direction(CURRENT_POINT, first_der);
			glPopMatrix();
		}

	}

	glutSwapBuffers();
}

void show_spiral() {
//	glLoadIdentity();
//	camera_position.set_view();

	glBegin(GL_POINTS);
	glColor3f(1,0,0);
	for(vec3 v : spiral->path()) {
		glVertex3f(v.x, v.y, v.z);
	}
	glEnd();
}

mat3 calc_show_orientation(vec3 CURRENT_POINT) {
	vec3 first_der = glm::normalize(spiral->derivation(animationState.getT(), animationState.getSeg()));
	vec3 secon_der = glm::normalize(spiral->second_derivation(animationState.getT(), animationState.getSeg()));

//	glLoadIdentity();
//	camera_position.set_view();

//	glScaled(5,5,5);
	glTranslatef(CURRENT_POINT.x, CURRENT_POINT.y, CURRENT_POINT.z);
	glScalef(5, 5, 5);

	glBegin(GL_LINES);
	//w
	glColor3f(0, 1, 0);
	vec3 w = first_der;
	glVertex3f(0,0,0);
	glVertex3f(w.x, w.y, w.z);
	//u
	vec3 u = glm::normalize(glm::cross(first_der, secon_der));
	glColor3f(1,1,1);
	glVertex3f(0,0,0);
	glVertex3f(u.x, u.y, u.z);

	//v
	vec3 v = glm::normalize(glm::cross(w, u));
	glColor3f(0,1,1);
	glVertex3f(0,0,0);
	glVertex3f(v.x, v.y, v.z);

	glEnd();

	return glm::inverse(mat3(w, u, v));

}

void show_plane(vec3 CURRENT_POINT, mat3 rotation) {
	glLoadIdentity();
	camera_position.set_view();

	glTranslatef(CURRENT_POINT.x, CURRENT_POINT.y, CURRENT_POINT.z);
	glRotated(-90, 0, 0, 1);
	glScaled(3,3,3);


	glBegin(GL_TRIANGLES);

	/*
	 * Preko https://www.creators3d.com/online-viewer sam shvatio
	 * da je model zapravo rotiran :)
	 */

	glColor3f(1.0f, 0.0f, 0.0f);

	for (vec3 connection : connections) {
		int x = connection.x - 1;
		int y = connection.y - 1;
		int z = connection.z - 1;

		vec3 t1 = vertices.at(x) * rotation;
		vec3 t2 = vertices.at(y) * rotation;
		vec3 t3 = vertices.at(z) * rotation;

		glVertex3f(t1.x, t1.y, t1.z);
		glVertex3f(t2.x, t2.y, t2.z);
		glVertex3f(t3.x, t3.y, t3.z);
	}
	glEnd();

}

void show_plane_w_direction(vec3 CURRENT_POINT, vec3 direction) {
	glLoadIdentity();
	camera_position.set_view();

	glTranslatef(CURRENT_POINT.x, CURRENT_POINT.y, CURRENT_POINT.z);
//	glRotated(-90, 0, 0, 1);

	vec3 current_direction(0,0,1);
	vec3 rotation_direction = glm::normalize(cross(current_direction, direction));

	double angle = acos(glm::dot(current_direction, direction) / glm::l2Norm(direction));
	angle = angle * 180 / M_PI;

	glRotated(angle, rotation_direction.x, rotation_direction.y, rotation_direction.z);
	glScaled(3,3,3);

	glBegin(GL_TRIANGLES);

	/*
	 * Preko https://www.creators3d.com/online-viewer sam shvatio
	 * da je model zapravo rotiran :)
	 */

	glColor3f(1.0f, 0.0f, 0.0f);

	for (vec3 connection : connections) {
		int x = connection.x - 1;
		int y = connection.y - 1;
		int z = connection.z - 1;



		vec3 t1 = vertices.at(x);
		vec3 t2 = vertices.at(y);
		vec3 t3 = vertices.at(z);

		glVertex3f(t1.x, t1.y, t1.z);
		glVertex3f(t2.x, t2.y, t2.z);
		glVertex3f(t3.x, t3.y, t3.z);
	}
	glEnd();

}



void reshape(GLsizei width, GLsizei height) {  // GLsizei for non-negative integer
	// Compute aspect ratio of the new window
	if (height == 0) height = 1;                // To prevent divide by 0
	GLfloat aspect = (GLfloat)width / (GLfloat)height;

	// Set the viewport to cover the new window
	glViewport(0, 0, width, height);

	// Set the aspect ratio of the clipping volume to match the viewport
	glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
	glLoadIdentity();             // Reset
	// Enable perspective projection with fovy, aspect, zNear and zFar
	gluPerspective(30.0f, aspect, 0.1f, 1000.0f);
}

int main(int argc, char **argv) {

	string obj_file_path;
	string path_file_path;
	if (argc == 3) {
		obj_file_path = argv[1];
		path_file_path = argv[2];
	} else {
		obj_file_path = "../objects/747.obj";
		path_file_path = "../paths/spiral.path";
	}

	ifstream obj_file;
	obj_file.open(obj_file_path);
	parse_data(&obj_file, &vertices, &connections);
	obj_file.close();

	//parse path
	spiral = new UniformBSplinePath(path_file_path);
	spiral->points();
	spiral->path();

	/**
	 *
void myMouseClickFunction(int button, int state, int x, int y) {
	cout << "Klik: " << button << endl;
}

void myMouseMotionFunction(int x, int y) {

	 */

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(50, 50);
	glutCreateWindow(title);
	glutMouseFunc(myMouseClickFunction);
	glutMotionFunc(myMouseMotionFunction);
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	initGL();
	glutIdleFunc(idleFunction);
	glutMainLoop();
	return 0;
}
