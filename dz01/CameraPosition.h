#ifndef RACGRA_01_CAMERAPOSITION_H
#define RACGRA_01_CAMERAPOSITION_H

#include <glm/common.hpp>

using namespace glm;

class CameraPosition {
	vec3 position;
	vec3 view_direction;
	double translation_speed;
	double rotation_speed;
public:
	CameraPosition(vec3, vec3, double, double);

	void set_view();

	void translate(int, int, int);
};



#endif //RACGRA_01_CAMERAPOSITION_H
