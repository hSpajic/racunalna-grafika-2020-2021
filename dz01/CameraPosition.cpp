#include "CameraPosition.h"
#include "GL/glut.h"

using namespace std;

CameraPosition::CameraPosition(vec3 p, vec3 vd, double ts, double rs) {
	position = p;
	view_direction = vd;
	translation_speed = ts;
	rotation_speed = rs;
}

void CameraPosition::translate(int x, int y, int z) {
	position[0] += x * translation_speed;
	position[1] += y * translation_speed;
	position[2] += z * translation_speed;
}

void CameraPosition::set_view() {
	gluLookAt(position[0], position[1], position[2], view_direction[0], view_direction[1], view_direction[2], 0, 0, 1);
}
