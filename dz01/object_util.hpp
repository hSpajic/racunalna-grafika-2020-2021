#ifndef OBJECT_UTIL_HPP
#define OBJECT_UTIL_HPP

#include <vector>
#include <fstream>
#include <glm/vec3.hpp>

using namespace std;

using vec3 = glm::vec3;

void parse_data(ifstream * file_stream, vector<vec3> * vertices, vector<vec3> * connections);

#endif